﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DASAdminMobile.Classes.Interface
{
    public interface IRegisterNotification
    {
        void DoRegister();
        void UnRegister();
    }
}
