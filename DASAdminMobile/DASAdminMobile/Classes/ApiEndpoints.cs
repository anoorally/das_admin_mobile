﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DASAdminMobile.Classes
{
    public static class ApiEndpoints
    {
        public static string ApiHost = "http://admin.dasgateway.com/";
        public static string ApiServiceHost = "https://api.dasgateway.com/v1/";

        //API Service Host usage
        public static string CheckCredentials { get; set; } = ApiHost + "Account/CheckCredentials";

        //API Host usage
        public static string LoginMobile { get; set; } = ApiHost + "Account/LoginMobile";


    }
}
