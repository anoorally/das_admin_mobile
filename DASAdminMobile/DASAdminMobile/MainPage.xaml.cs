﻿using DASAdminMobile.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DASAdminMobile
{
	public partial class MainPage : ContentPage
	{
        public MainPage(string url)
        {
            InitializeComponent();

            Settings.NotificationCount = 0;

            myWebView.Navigating += MyWebView_Navigating;

            myWebView.Source = url;


        }

        private void MyWebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            if (e.Url.ToLower().Contains("logout") || e.Url.ToLower().Contains("logoff"))
            {
                //Navigation.PopAsync();

                App.Current.MainPage = new Login();

            }
        }

    }
}
