
 //Helpers/Settings.cs This file was automatically added when you installed the Settings Plugin.If you are not using a PCL then comment this file back in to use it.
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DASAdminMobile.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications.All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>

    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        #endregion


        public static string AccountName
        {
            get
            {
                return AppSettings.GetValueOrDefault("AccountName", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("AccountName", value);
            }
        }

        public static string Username
        {
            get
            {
                return AppSettings.GetValueOrDefault("Username", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("Username", value);
            }
        }

        public static string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault("Password", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("Password", value);
            }
        }

        public static string ContactEmail
        {
            get
            {
                return AppSettings.GetValueOrDefault("ContactEmail", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("ContactEmail", value);
            }
        }

        public static string CustomerID
        {
            get
            {
                return AppSettings.GetValueOrDefault("CustomerID", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("CustomerID", value);
            }
        }


        public static string NotificationRegID
        {
            get
            {
                return AppSettings.GetValueOrDefault("NotificationRegID", SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue("NotificationRegID", value);
            }
        }

        public static int NotificationCount
        {
            get
            {
                return AppSettings.GetValueOrDefault("NotificationCount", 0);
            }
            set
            {
                AppSettings.AddOrUpdateValue("NotificationCount", value);
            }
        }


    }
}