﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DASAdminMobile.Helpers
{
    public static class Functions
    {

        public static int getDeviceWidth()
        {
            //int intHeight = 0;
            int intWidth = 0;



#if __ANDROID__
	        //intHeight = (int)(Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.HeightPixels / Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density);
	        intWidth = (int) (Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.WidthPixels / Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density);
#endif

#if __IOS__
	//intHeight = Convert.ToInt32(UIKit.UIScreen.MainScreen.Bounds.Height); 
	intWidth = Convert.ToInt32(UIKit.UIScreen.MainScreen.Bounds.Width);
#endif

#if WINDOWS_PHONE_APP
	//intHeight = Window.Current.Bounds.Height; 
	intWidth = Window.Current.Bounds.Width;
#endif

            return intWidth;
        }

        public static int getDeviceHeight()
        {
            int intHeight = 0;

#if __ANDROID__
	        intHeight = (int) (Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.HeightPixels / Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density);
#endif

#if __IOS__
            intHeight = Convert.ToInt32(UIKit.UIScreen.MainScreen.Bounds.Height);
#endif

#if WINDOWS_PHONE_APP
	intHeight = Window.Current.Bounds.Height; 
#endif

            return intHeight;
        }

        public static bool isSmallScreen()
        {
            if (getDeviceWidth()<=320)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool isSmallScreenAndiOS()
        {
            if (isSmallScreen() && Device.RuntimePlatform == Device.iOS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //public static void ScaleInStack(StackLayout stack) {
        //    stack.Scale = 0;
        //    stack.ScaleTo(1, 100, Easing.BounceIn);
        //}

        public static void StackSlideIn(StackLayout stack, int millisecond=300)
        {
            //var cmd = new Command(async ()=> {
            //    await StackSlideIn_main(stack, millisecond);
            //});cmd.Execute(null);

            stack.Opacity = 0;

            stack.FadeTo(1, (uint)millisecond, Easing.Linear);
        }

        public static double FontLarge() {
            return Device.GetNamedSize(NamedSize.Large, typeof(Label));
        }

        public static double FontMedium()
        {
            return Device.GetNamedSize(NamedSize.Medium, typeof(Label));
        }

        public static double FontSmall()
        {
            return Device.GetNamedSize(NamedSize.Small, typeof(Label));
        }


        //public static Task StackSlideIn_main(StackLayout stack, int millisecond = 250)
        //{
        //        double finalHeight = stack.Height;
        //        stack.HeightRequest = 0;

        //        int count = 0;

        //        double diff = finalHeight / millisecond;

        //        while (count < millisecond)
        //        {
        //            stack.HeightRequest += diff;
        //            count++;
        //            Thread.Sleep(1);
        //        }
        //        stack.HeightRequest = finalHeight;

        //    return null;

        //stack.ScaleTo(1, (uint)millisecond, Easing.Linear);
        //}
    }
}
