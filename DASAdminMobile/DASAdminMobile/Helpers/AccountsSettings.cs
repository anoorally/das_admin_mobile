﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Auth;

namespace DASAdminMobile.Helpers
{
    public static class AccountsSettings
    {

        public static Account Account {
            get
            {
                return AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

            }
            set{}
        }

        public static string AccountName
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var notifID = account.Properties["AccountName"];
                return notifID;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

                if (account != null)
                {
                    account.Properties["AccountName"] = value;
                }


            }
        }

        public static string Username {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                return (account != null) ? account.Username : null;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                account.Username = value;
            }
        }

        public static string AccessToken
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var access_token = account.Properties["access_token"];
                return access_token;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                account.Properties["access_token"] = value;
            }
        }

        public static string TokenType
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var token_type = account.Properties["token_type"];
                return token_type;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                account.Properties["token_type"] = value;
            }
        }

        public static string Expiry
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var expires_in = account.Properties["expires_in"];
                return expires_in;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                account.Properties["expires_in"] = value;
            }
        }

        public static string CustId
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var cust = account.Properties["CustId"];
                return cust;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                account.Properties["CustId"] = value;
            }
        }


        public static string NotificationTokenID {
            get {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var notifID = account.Properties["NotificationTokenID"];
                return notifID;
            }
            set {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

                if (account != null) {
                    account.Properties["NotificationTokenID"] = value;
                }


            }
        }

        public static string Password
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var notifID = account.Properties["Password"];
                return notifID;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

                if (account != null)
                {
                    account.Properties["Password"] = value;
                }


            }
        }

        public static string RememberMe
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                var remember = account.Properties["RememberMe"];
                return remember;
            }
            set
            {
                var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

                if (account != null)
                {
                    account.Properties["RememberMe"] = value;
                }


            }
        }


        public static void clearAccounts()
        {
            //Delete existing account-record entries to save new eneters ones
            while (AccountStore.Create().FindAccountsForService(App.Current.ToString()).Count() > 0)
            {
                var delAccount = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();
                if (delAccount != null)
                {
                    AccountStore.Create().Delete(delAccount, App.Current.ToString());
                }
            }
        }
        

        public static void createAccount(string accountName, string username, string password, string rememberme)
        {
            //Clear before adding to have only 1 account entry
            clearAccounts();

            //Create new instance to save records
            Account account = new Account
            {
                Username = username
            };


            account.Properties["AccountName"] = accountName;
            account.Properties["Username"] = username;
            account.Properties["Password"] = password;
            account.Properties["RememberMe"] = rememberme;

            //Save the account record
            AccountStore.Create().Save(account, App.Current.ToString());
        }


        public static bool addProperty(string Name, string propValue)
        {

            List<KeyValuePair<string, string>> temp = new List<KeyValuePair<string, string>>() ;

            //Get the account
            var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

            if (account != null) {
                //Save all info
                var username = account.Username;
                foreach (var p in account.Properties) {
                    temp.Add(new KeyValuePair<string, string>(p.Key, p.Value));
                }
                temp.Add(new KeyValuePair<string, string>(Name, propValue));

                //Delete all accounts
                clearAccounts();

                //Create new account with properties

                Account acc = new Account
                {
                    Username = username
                };

                foreach (var entry in temp) {
                    account.Properties[entry.Key] = entry.Value;
                }

                //Save the account record
                AccountStore.Create().Save(account, App.Current.ToString());
                return true;
            }
            else {
                return false;
            }
        }

    }
}
