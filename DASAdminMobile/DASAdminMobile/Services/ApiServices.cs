﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.ComTypes;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using DASAdminMobile.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using DASAdminMobile.Classes;
using DASAdminMobile.Helpers;
using System.Globalization;
using DASAdminMobile;
using Xamarin.Forms;

namespace DASAdminMobile.Services
{
    public class ApiServices
    {

        public async Task<bool> LoginAsync(string accountname, string username, string password)
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string res;
            HttpResponseMessage response = client.GetAsync(ApiEndpoints.CheckCredentials + "?a="+ accountname + "&u=" + username + "&p=" + password).Result;
            if (response.IsSuccessStatusCode)
            {
                res = await response.Content.ReadAsStringAsync();
            }
            else
            {
                //Something has gone wrong, handle it here
                throw new Exception();
            }


            //var content = await response.Content.ReadAsStringAsync();

            //bool res = false;


            bool result = false; 
            try
            {
                result = bool.Parse(res.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            //if (response.IsSuccessStatusCode)
            //    return content;
            //else
            //    return string.Empty;

            return result;


        }

        public async Task<string> CheckCredentials(string accountName, string username, string password)
        {
            var client = new HttpClient();

            //var postValues = new List<KeyValuePair<string, string>>
            //{
            //    new KeyValuePair<string, string>("Username", username),
            //    new KeyValuePair<string, string>("Password", password),
            //    new KeyValuePair<string, string>("grant_type", "password")
            //};

            var request = new HttpRequestMessage(HttpMethod.Get, ApiEndpoints.CheckCredentials + "?a=" + accountName + "&u=" + username + "&p=" + password);
            //request.Content = new FormUrlEncodedContent(postValues);


            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            var response = await client.SendAsync(request);



            var content = await response.Content.ReadAsStringAsync();


            if (response.IsSuccessStatusCode)
                return content;
            else
                return string.Empty;

        }


    }

}

