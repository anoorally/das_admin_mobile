﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using DASAdminMobile.Helpers;
using DASAdminMobile.Services;
using Xamarin.Auth;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Plugin.Vibrate;
using DASAdminMobile.Classes;

namespace DASAdminMobile.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        ApiServices _apiServices = new ApiServices();

        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }

        public string AccountName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public string Expiry { get; set; }

        public bool _RememberMe;
        public bool RememberMe
        {
            get { return _RememberMe; }
            set
            {
                _RememberMe = value;
                OnPropertyChanged("RememberMe");
            }
        }

        public bool IsLoginSuccess { get; set; }

        public bool _PageIsBusy;
        public bool PageIsBusy
        {
            get { return _PageIsBusy; }
            set
            {
                _PageIsBusy = value;
                IsClickable = !_PageIsBusy;
                OnPropertyChanged("PageIsBusy");
            }
        }


        public bool _IsClickable { get; set; } = true;
        public bool IsClickable
        {
            get { return _IsClickable; }
            set
            {
                _IsClickable = value;
                OnPropertyChanged(nameof(IsClickable));
            }
        }


        public LoginViewModel(INavigation navigation)
        {

            //Get Navigation handle
            Navigation = navigation;

            _PageIsBusy = false;

            var account = AccountStore.Create().FindAccountsForService(App.Current.ToString()).FirstOrDefault();

            if (account != null)
            {
                if (bool.Parse(AccountsSettings.RememberMe))
                {
                    AccountName = AccountsSettings.AccountName;
                    Username = AccountsSettings.Username;
                    Password = AccountsSettings.Password;
                    RememberMe = bool.Parse(AccountsSettings.RememberMe);
                }
            }


        }


        public ICommand LoginCommand
        {
            get
            {
                return new Command(async () =>
                {

                    if (CrossConnectivity.Current.IsConnected)
                    {
                        //var apiResponse = await _apiServices.LoginAsync(Username, Password);
                        var apiResponse = await _apiServices.CheckCredentials(AccountName, Username, Password);
                        var loginSucceeded = bool.Parse(apiResponse);


                        //Show spinner
                        PageIsBusy = true;



                        if (loginSucceeded)
                        {

                            AccountsSettings.createAccount(AccountName, Username, Password, RememberMe.ToString());

                            //if (RememberMe)
                            //{
                            //    AccountsSettings.Username = Username;
                            //    AccountsSettings.Password = Password;
                            //    AccountsSettings.RememberMe = RememberMe.ToString(); ;
                            //}

                            DependencyService.Get<Classes.Interface.IRegisterNotification>().DoRegister();

                            CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 25));
                            //Navigate to Dashboard
                            App.Current.MainPage = new MainPage(ApiEndpoints.LoginMobile + "?a="+ AccountName + "&u=" + Username + "&p=" + Password);


                        }
                        else
                        {
                            PageIsBusy = false;
                            await App.Current.MainPage.DisplayAlert("Login", "Incorrect username and/or password.", "Close");

                        }

                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Connection", "No Internet connection detected.\nPlease try again.", "Okay");
                        PageIsBusy = false;

                    }

                });
            }
        }




        private void OnPropertyChanged(string propertyName)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

    }

}
