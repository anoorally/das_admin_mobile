﻿using System;
using DASAdminMobile.Services;
using DASAdminMobile.ViewModel;
using Plugin.Connectivity;
using Plugin.Vibrate;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DASAdminMobile.Classes;
using DASAdminMobile.Helpers;

namespace DASAdminMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {

        private LoginViewModel vm;
        public Login()
        {
            InitializeComponent();

            //var width = DASAdminMobile.Helpers.Functions.isSmallScreen();

            //Set as root
            App.Current.MainPage = this;

            txtUname.Text = AccountsSettings.Username;

            vm = new LoginViewModel(Navigation);

            this.BindingContext = vm;

            txtAccountName.Completed += (object sender, EventArgs e) =>
            {
                txtUname.Focus();
            };


            txtUname.Completed += (object sender, EventArgs e) =>
            {
                txtPass.Focus();
            };

            txtPass.Completed += (object sender, EventArgs e) => {
                runner.IsRunning = true;
                vm.LoginCommand.Execute(null);
            };

            //LoginEntry x = new LoginEntry();

            //loginPage.Children.Add(x);
        }


        //private async void guestLogin_Clicked(object sender, EventArgs e)
        //{
        //    //var newMain = new SamplePage();

        //    var newMain = new Dashboard();
        //    await App.Current.MainPage.Navigation.PushModalAsync(newMain, true);

        //    //Set newMain as root of a Navigation instance
        //    App.Current.MainPage = new NavigationPage(newMain);

        //}

        private void cmdLogin_Clicked(object sender, EventArgs e)
        {
            runner.IsRunning = true;
            cmdLogin.IsEnabled = false;

            //myWebView.Source = ApiEndpoints.LoginMobile + "?u=" + txtUname.Text + "&p=" + txtPass.Text;
        }

        //private void cmdForgotPassword_Clicked(object sender, EventArgs e)
        //{
        //    Command cmd = new Command(async ()=> {
        //        if (CrossConnectivity.Current.IsConnected)
        //        {
        //            if (!string.IsNullOrWhiteSpace(txtUname.Text))
        //            {
        //                bool userChoice = await DisplayAlert("Password reset", "This will reset your current DASAdminMobile password.\nDo you confirm?", "Yes", "No");
        //                if (userChoice)
        //                {
        //                    ApiServices _api = new ApiServices();
        //                    var ok = await _api.ForgetPasswordService(txtUname.Text);
        //                    if (ok)
        //                    {
        //                        await DisplayAlert("Password reset", "An email has been sent to\n " + txtUname.Text + "\nwith further instructions to reset your password.", "OK");
        //                    }
        //                    else
        //                    {
        //                        await DisplayAlert("Password reset", "Error occured in sending the password reset request.", "OK");
        //                    }
        //                }

        //            }
        //        }
        //        else {
        //            await DisplayAlert("Network", "No Internet access has been detected.\nPlease check your connection.", "OK");
        //        }
                
                
        //    });cmd.Execute(null);
        //}



        //Testing
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e) {
            //check();
            try {
                await DASAdminMobileLogoImage.RelRotateTo(360, 500, Easing.CubicOut); await DASAdminMobileLogoImage.RotateTo(0, 0);
            }
            catch (Exception) {
                
                throw;
            }

        }


        //private async void cmdNavigated(object sender, WebNavigatedEventArgs e)
        //{
        //    string navUrl = e.Url;

        //    if (navUrl.ToLower().Contains("unauthorized"))
        //    {
        //        runner.IsRunning = false;
        //        cmdLogin.IsEnabled = true;

        //        await App.Current.MainPage.DisplayAlert("Login", "Incorrect username and/or password.", "Close");
        //    }
        //    else {
        //        //AccountsSettings.Username = txtUname.Text;
        //        CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 25));
        //        App.Current.MainPage = new MainPage(ApiEndpoints.LoginMobile + "?u=" + txtUname.Text + "&p=" + txtPass.Text);
        //    }

        //}
        //public async void check()
        //{

        //    try {
        //        var res = await CrossFingerprint.Current.IsAvailableAsync(true);
        //        if (res)
        //        {
        //            try
        //            {
        //                if (txtPass.Text == "test") {
        //                    CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 10));
        //                    var result = await CrossFingerprint.Current.AuthenticateAsync("");
        //                    if (result.Authenticated)
        //                    {
        //                            App.Current.MainPage = new NavigationPage(new Dashboard());
        //                    }
        //                }    
        //            }
        //            catch (Exception){}
        //        }
        //    }
        //    catch (Exception) {
        //        throw;
        //    }

        //}

    }
}