﻿
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Util;
using DASAdminMobile.Helpers;
using Gcm.Client;
using Java.Util;
using ME.Leolin.Shortcutbadger;
using Plugin.Settings;
using Plugin.Vibrate;
using System;
using System.Collections.Generic;
using System.Text;
using WindowsAzure.Messaging;

[assembly: Permission(Name = "@PACKAGE_NAME@.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "@PACKAGE_NAME@.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.google.android.c2dm.permission.RECEIVE")]

//GET_ACCOUNTS is needed only for Android versions 4.0.3 and below
[assembly: UsesPermission(Name = "android.permission.GET_ACCOUNTS")]
[assembly: UsesPermission(Name = "android.permission.INTERNET")]
[assembly: UsesPermission(Name = "android.permission.WAKE_LOCK")]

namespace DASAdminMobile.Droid.Helpers
{
    [BroadcastReceiver(Permission = Gcm.Client.Constants.PERMISSION_GCM_INTENTS)]
    [IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_MESSAGE },
    Categories = new string[] { "@PACKAGE_NAME@" })]
    [IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK },
    Categories = new string[] { "@PACKAGE_NAME@" })]
    [IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_LIBRARY_RETRY },
    Categories = new string[] { "@PACKAGE_NAME@" })]
    public class MyBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
    {
        public static string[] SENDER_IDS = new string[] { Classes.Constants.SenderID };

        public const string TAG = "MyBroadcastReceiver-GCM";
    }



    [Service] // Must use the service tag
    public class PushHandlerService : GcmServiceBase
    {
        public static string RegistrationID { get; private set; }
        private NotificationHub Hub { get; set; }

        public PushHandlerService() : base(Classes.Constants.SenderID)
        {
            Log.Info(MyBroadcastReceiver.TAG, "PushHandlerService() constructor");
        }

        protected override void OnRegistered(Context context, string registrationId)
        {
            Log.Verbose(MyBroadcastReceiver.TAG, "GCM Registered: " + registrationId);


            RegistrationID = registrationId;
            //Also save in accountSettings
            AccountsSettings.NotificationTokenID = RegistrationID;


            Settings.NotificationRegID = RegistrationID;


            //createNotification("PushHandlerService-GCM Registered...", "The device has been Registered!");

            Hub = new NotificationHub(Classes.Constants.NotificationHubName, Classes.Constants.ListenConnectionString,
                                        context);
            try
            {
                Hub.UnregisterAll(registrationId);
            }
            catch (Exception ex)
            {
                Log.Error(MyBroadcastReceiver.TAG, ex.Message);
            }

            var tags = new List<string>() { };

            //Register by tag
            if (!string.IsNullOrEmpty(Settings.Username)){
                tags.Add(Settings.Username);
            }

            tags.Add("admin");//add admin tag





            try
            {
                var hubRegistration = Hub.Register(registrationId, tags.ToArray());
            }
            catch (Exception ex)
            {
                Log.Error(MyBroadcastReceiver.TAG, ex.Message);
            }
        }


        protected override void OnMessage(Context context, Intent intent)
        {
            Log.Info(MyBroadcastReceiver.TAG, "GCM Message Received!");

            var msg = new StringBuilder();

            if (intent != null && intent.Extras != null)
            {
                foreach (var key in intent.Extras.KeySet())
                    msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
            }

            string messageText = intent.Extras.GetString("message");

            //Set default title if not exists
            string messageTitle = (intent.HasExtra("title") ? intent.Extras.GetString("title") : "DASGateway");



            if (!string.IsNullOrEmpty(messageText))
            {
                createNotification(messageTitle, messageText);

            }
            else
            {
                createNotification("Unknown message details", msg.ToString());
            }
            

        }


        void createNotification(string title, string desc)
        {
            //Create notification
            var notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;

            //Create an intent to show UI
            var uiIntent = new Intent(this, typeof(MainActivity));
            //var uiIntent = new Intent(this, typeof(DASAdminMobile.View.NotificationPage));




            //Create the notification
            //var notification = new Notification(Android.Resource.Drawable.SymActionEmail, title);

            Notification notification;




            //Auto-cancel will remove the notification once the user touches it

            //Set the notification info
            //we use the pending intent, passing our ui intent over, which will get called
            //when the notification is tapped.
            //notification.SetLatestEventInfo(this, title, desc, PendingIntent.GetActivity(this, 0, uiIntent, 0));


            //var sdkVersion = Integer.parseInt(Build.VERSION.Sdk);


            if (Convert.ToInt32(Build.VERSION.Sdk) < (int)BuildVersionCodes.Honeycomb)
            {
                notification = new Notification(Resource.Drawable.notif_icon, title)
                {
                    Flags = NotificationFlags.AutoCancel
                };
                notification.SetLatestEventInfo(this, title, desc, PendingIntent.GetActivity(this, 0, uiIntent, 0));
                notification.Priority = (int)NotificationPriority.High;

                Context context = Application.Context;
               
            }
            else
            {
                notification = new Notification.Builder(this)
                .SetPriority((int)NotificationPriority.Max)
                .SetContentTitle(title)
                .SetContentText(desc)
                .SetVibrate(new long[] { 200 })
                .SetLights(Color.Rgb(0, 174, 239), 2000, 2000)
                .SetSmallIcon(Resource.Drawable.notif_icon)
                .SetAutoCancel(true)
                .SetContentIntent(PendingIntent.GetActivity(this, 0, uiIntent, 0))
                .Build();
            }




            CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 200));
            //DASAdminMobile.Classes.Constants.LastNotification = desc;


            //Settings.AddNotificationMessage(desc);
            Settings.NotificationCount += 1;




            //Thread t = new Thread(() =>
            //{
            //    try
            //    {
            //        CrossSettings.Current.AddOrUpdateValue("NotificationMessage", desc);
            //        CrossSettings.Current.AddOrUpdateValue("NotificationCount", CrossSettings.Current.GetValueOrDefault("NotificationCount", 0) + 1);
            //        //Xamarin.Forms.Application.Current.Properties["NotificationMessage"] = desc;
            //        //Xamarin.Forms.Application.Current.Properties["NotificationCount"] = Convert.ToInt32(Xamarin.Forms.Application.Current.Properties["NotificationCount"]) + 1;
            //        //Xamarin.Forms.Application.Current.SavePropertiesAsync();
            //    }
            //    finally
            //    {
            //        Console.WriteLine("finished executing ThreadProc");
            //    }
            //});
            //t.IsBackground = true;
            //t.Start();



            try
            {
                //Badge.Plugin.CrossBadge.Current.SetBadge(CrossSettings.Current.GetValueOrDefault("NotificationCount", 0));
                //Badge.Plugin.CrossBadge.Current.SetBadge(10);
                ShortcutBadger.ApplyCount(Application.Context, CrossSettings.Current.GetValueOrDefault("NotificationCount", 0));


            }
            catch (Exception ex)
            {
                //Functions.LogAdd(ex.Message, this.GetType().Name);
            }

            //Show the notification
            notificationManager.Notify((int)new Date().Time, notification);
            //notificationManager.Notify((int)new Date().Time, notification);
            //dialogNotify(title, desc);
        }

        protected void dialogNotify(String title, String message)
        {

            MainActivity.instance.RunOnUiThread(() =>
            {
                AlertDialog.Builder dlg = new AlertDialog.Builder(MainActivity.instance);
                AlertDialog alert = dlg.Create();
                alert.SetTitle(title);
                alert.SetButton("Ok", delegate
                {
                    alert.Dismiss();
                });
                alert.SetMessage(message);
                alert.Show();
            });


        }

        protected override void OnUnRegistered(Context context, string registrationId)
        {
            //Log.Verbose(MyBroadcastReceiver.TAG, "GCM Unregistered: " + registrationId);

            //createNotification("GCM Unregistered...", "The device has been unregistered!");
        }

        protected override bool OnRecoverableError(Context context, string errorId)
        {
            Log.Warn(MyBroadcastReceiver.TAG, "Recoverable Error: " + errorId);

            return base.OnRecoverableError(context, errorId);
        }

        protected override void OnError(Context context, string errorId)
        {
            Log.Error(MyBroadcastReceiver.TAG, "GCM Error: " + errorId);
        }

    }
}