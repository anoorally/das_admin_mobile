﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DASAdminMobile.Classes.Interface;
using Gcm.Client;
using Android.Util;
using DASAdminMobile.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(RegisterNotification))]
namespace DASAdminMobile.Droid
{

    public class RegisterNotification : IRegisterNotification
    {
        public void DoRegister()
        {
            RegisterWithGCM();
        }


        public void UnRegister()
        {
            var context = Application.Context;
            GcmClient.UnRegister(context);
        }

        private void RegisterWithGCM()
        {
            try
            {
                var context = Application.Context;

                // Check to ensure everything's set up right
                GcmClient.CheckDevice(context);
                GcmClient.CheckManifest(context);

                // Register for push notifications
                Log.Info("MainActivity", "Registering...");
                GcmClient.Register(context, Classes.Constants.SenderID);
            }
            catch (Exception ex)
            {
                Console.WriteLine("======>!!!ERROR!!!: " + ex.Message);
            }
            
        }


    }
}