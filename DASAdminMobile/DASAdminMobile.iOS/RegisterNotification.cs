﻿using Foundation;
using DASAdminMobile.Classes;
using DASAdminMobile.Classes.Interface;
using DASAdminMobile.Droid;
using System;
using UIKit;
using UserNotifications;
using WindowsAzure.Messaging;

[assembly: Xamarin.Forms.Dependency(typeof(RegisterNotification))]
namespace DASAdminMobile.Droid
{

    public class RegisterNotification :  Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IRegisterNotification
    {

        private SBNotificationHub Hub { get; set; }

        public void DoRegister()
        {
            //try
            //{
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                       UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                       new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            //}
            //catch (System.Exception)
            //{

            //    throw;
            //}

            //if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            //{
            //    // iOS 10 or later
            //    var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
            //    UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
            //    {
            //        if (granted)
            //        {
            //            InvokeOnMainThread(() =>
            //            {
            //                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            //            });
            //        }
            //    });

            //    // For iOS 10 display notification (sent via APNS)
            //    //UNUserNotificationCenter.Current.Delegate = ADSelf;
            //}
            //else
            //{
            //    // iOS 9 or before
            //    var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
            //    var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
            //    UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            //}

        }

        public void UnRegister()
        {
            UIApplication.SharedApplication.UnregisterForRemoteNotifications();
        }



        //public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        //{
        //    Hub = new SBNotificationHub(Constants.ListenConnectionString, Constants.NotificationHubName);

        //    Hub.UnregisterAllAsync(deviceToken, (error) =>
        //    {
        //        if (error != null)
        //        {
        //            Console.WriteLine("Error calling Unregister: {0}", error.ToString());
        //            return;
        //        }

        //        NSSet tags = null; // create tags if you want
        //        Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) =>
        //        {
        //            if (errorCallback != null)
        //                Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
        //        });
        //    });
        //}

        //public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        //{
        //    ProcessNotification(userInfo, false);
        //}

        //void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        //{
        //    Console.WriteLine("###################################GOT HERE");

        //    // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
        //    if (null != options && options.ContainsKey(new NSString("aps")))
        //    {
        //        //Get the aps dictionary
        //        NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

        //        string alert = string.Empty;
        //        string title = "DASAdminMobile";

        //        //Extract the alert text
        //        // NOTE: If you're using the simple alert by just specifying
        //        // "  aps:{alert:"alert msg here"}  ", this will work fine.
        //        // But if you're using a complex alert with Localization keys, etc.,
        //        // your "alert" object from the aps dictionary will be another NSDictionary.
        //        // Basically the JSON gets dumped right into a NSDictionary,
        //        // so keep that in mind.
        //        if (aps.ContainsKey(new NSString("message")))
        //            alert = (aps[new NSString("message")] as NSString).ToString();

        //        if (aps.ContainsKey(new NSString("title")))
        //            alert = (aps[new NSString("title")] as NSString).ToString();

        //        //If this came from the ReceivedRemoteNotification while the app was running,
        //        // we of course need to manually process things like the sound, badge, and alert.
        //        if (!fromFinishedLaunching)
        //        {
        //            //Manually show an alert
        //            if (!string.IsNullOrEmpty(alert))
        //            {
        //                //UIAlertView avAlert = new UIAlertView(title: title, message: alert, del: null, cancelButtonTitle: "OK", otherButtons: null);
        //                UIAlertView avAlert = new UIAlertView(title, alert, null, "OK", null);

        //                avAlert.Show();
        //            }
        //        }
        //    }
        //}


    }
}