﻿using System;
using System.Collections.Generic;
using System.Linq;
using DASAdminMobile.Classes;
using DASAdminMobile.Helpers;
using Foundation;
using UIKit;
using WindowsAzure.Messaging;

namespace DASAdminMobile.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        private SBNotificationHub Hub { get; set; }



        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }


        public void UnRegister()
        {
            UIApplication.SharedApplication.UnregisterForRemoteNotifications();
        }


        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Hub = new SBNotificationHub(Constants.ListenConnectionString, Constants.NotificationHubName);

            Hub.UnregisterAllAsync(deviceToken, (error) => {
                if (error != null)
                {
                    Console.WriteLine("Error calling Unregister: {0}", error.ToString());
                    //Settings.Logs += "Error calling Unregister: " + error.ToString() + "\n";
                    return;
                }

                //Create tags

                List<string> tagList = new List<string>();
                tagList.Add(Settings.Username); //Add username as tag
                tagList.Add("admin");//add Admin as tag

                ////TestMode tag
                //if (Functions.isTestMode)
                //{
                //    tagList.Add("testmode");
                //}

                ////Tags by TestPage
                //var moreTags = Functions.MoreTags;
                //if (!string.IsNullOrEmpty(moreTags))
                //{
                //    string[] arr = moreTags.Split(',');
                //    foreach (var tag in arr)
                //    {
                //        tagList.Add(tag.Trim());
                //    }
                //}


                //Compile Tag list
                NSSet tags = new NSSet(tagList.ToArray());

                Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) => {
                    if (errorCallback != null)
                    {
                        Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                        //Settings.Logs += "Error calling Unregister: " + error.ToString() + "\n";
                    }
                    else
                    {
                        Settings.NotificationRegID = deviceToken.ToString();
                    }

                });
            });
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            ProcessNotification(userInfo, false);
        }

        void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;
                string title = "DASGateway";

                //Extract the alert text
                // NOTE: If you're using the simple alert by just specifying
                // "  aps:{alert:"alert msg here"}  ", this will work fine.
                // But if you're using a complex alert with Localization keys, etc.,
                // your "alert" object from the aps dictionary will be another NSDictionary.
                // Basically the JSON gets dumped right into a NSDictionary,
                // so keep that in mind.
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();

                if (aps.ContainsKey(new NSString("title")))
                    title = (aps[new NSString("title")] as NSString).ToString();


                //Specific commands for testing
                //if (aps.ContainsKey(new NSString("cmd")))
                //{
                //    string command = (aps[new NSString("cmd")] as NSString).ToString();
                //    string[] keyValue = command.Split('~');
                //    Functions.SetStorage(keyValue[0], keyValue[1]);
                //}

                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching)
                {

                    //Manually show an alert
                    if (!string.IsNullOrEmpty(alert))
                    {
                        //UIAlertView avAlert = new UIAlertView(title: title, message: alert, del: null, cancelButtonTitle: "OK", otherButtons: null);
                        //avAlert.Show();


                        //Create Alert
                        var okAlertController = UIAlertController.Create(title, alert, UIAlertControllerStyle.Alert);


                        //Add Action
                        okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                        // Present Alert
                        var window = UIApplication.SharedApplication.KeyWindow;
                        var vc = window.RootViewController;
                        vc.PresentViewController(okAlertController, true, null);

                        //Settings.AddNotificationMessage(alert);
                        //Settings.NotificationCount += 1;
                    }


                    //string sound = string.Empty;
                    ////Extract the sound string
                    //if (aps.ContainsKey(new NSString("sound")))
                    //    sound = (aps[new NSString("sound")] as NSString).ToString();

                    ////Manually play the sound
                    //if (!string.IsNullOrEmpty(sound))
                    //{
                    //    //This assumes that in your json payload you sent the sound filename (like sound.caf)
                    //    // and that you've included it in your project directory as a Content Build type.
                    //    var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile(sound);
                    //    soundObj.PlaySystemSound();
                    //}
                }
            }
        }

        public static UIAlertController PresentOKAlert(string title, string description, UIViewController controller)
        {
            // No, inform the user that they must create a home first
            UIAlertController alert = UIAlertController.Create(title, description, UIAlertControllerStyle.Alert);

            // Configure the alert
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (action) => { }));

            // Display the alert
            controller.PresentViewController(alert, true, null);

            // Return created controller
            return alert;
        }
    }
}
